diff --git a/Database/Persist/TH.hs b/Database/Persist/TH.hs
index a10dbc8..74d3501 100644
--- a/Database/Persist/TH.hs
+++ b/Database/Persist/TH.hs
@@ -90,7 +90,6 @@ import Data.Either
 import qualified Data.HashMap.Strict as HM
 import Data.Int (Int64)
 import Data.Ix (Ix)
-import Data.List (foldl')
 import qualified Data.List as List
 import Data.List.NonEmpty (NonEmpty(..))
 import qualified Data.List.NonEmpty as NEL
@@ -108,7 +107,7 @@ import GHC.TypeLits
 import Instances.TH.Lift ()
     -- Bring `Lift (fmap k v)` instance into scope, as well as `Lift Text`
     -- instance on pre-1.2.4 versions of `text`
-import Data.Foldable (toList)
+import Data.Foldable (foldl', toList)
 import qualified Data.Set as Set
 import Language.Haskell.TH.Lib
        (appT, conE, conK, conT, litT, strTyLit, varE, varP, varT)
@@ -1284,7 +1283,7 @@ mkToPersistFields mps ed = do
     go = do
         xs <- sequence $ replicate fieldCount $ newName "x"
         let name = mkEntityDefName ed
-            pat = ConP name $ fmap VarP xs
+            pat = conPCompat name $ fmap VarP xs
         sp <- [|SomePersistField|]
         let bod = ListE $ fmap (AppE sp . VarE) xs
         return $ normalClause [pat] bod
@@ -1306,7 +1305,7 @@ mkToPersistFields mps ed = do
                 , [sp `AppE` VarE x]
                 , after
                 ]
-        return $ normalClause [ConP name [VarP x]] body
+        return $ normalClause [conPCompat name [VarP x]] body
 
 mkToFieldNames :: [UniqueDef] -> Q Dec
 mkToFieldNames pairs = do
@@ -1328,7 +1327,7 @@ mkUniqueToValues pairs = do
     go :: UniqueDef -> Q Clause
     go (UniqueDef constr _ names _) = do
         xs <- mapM (const $ newName "x") names
-        let pat = ConP (mkConstraintName constr) $ fmap VarP $ toList xs
+        let pat = conPCompat (mkConstraintName constr) $ fmap VarP $ toList xs
         tpv <- [|toPersistValue|]
         let bod = ListE $ fmap (AppE tpv . VarE) $ toList xs
         return $ normalClause [pat] bod
@@ -1367,7 +1366,7 @@ mkFromPersistValues mps entDef
     mkClauses _ [] = return []
     mkClauses before (field:after) = do
         x <- newName "x"
-        let null' = ConP 'PersistNull []
+        let null' = conPCompat 'PersistNull []
             pat = ListP $ mconcat
                 [ fmap (const null') before
                 , [VarP x]
@@ -1404,20 +1403,20 @@ mkLensClauses mps entDef = do
     valName <- newName "value"
     xName <- newName "x"
     let idClause = normalClause
-            [ConP (keyIdName entDef) []]
+            [conPCompat (keyIdName entDef) []]
             (lens' `AppE` getId `AppE` setId)
     return $ idClause : if unboundEntitySum entDef
         then fmap (toSumClause lens' keyVar valName xName) (getUnboundFieldDefs entDef)
         else fmap (toClause lens' getVal dot keyVar valName xName) (getUnboundFieldDefs entDef)
   where
     toClause lens' getVal dot keyVar valName xName fieldDef = normalClause
-        [ConP (filterConName mps entDef fieldDef) []]
+        [conPCompat (filterConName mps entDef fieldDef) []]
         (lens' `AppE` getter `AppE` setter)
       where
         fieldName = fieldDefToRecordName mps entDef fieldDef
         getter = InfixE (Just $ VarE fieldName) dot (Just getVal)
         setter = LamE
-            [ ConP 'Entity [VarP keyVar, VarP valName]
+            [ conPCompat 'Entity [VarP keyVar, VarP valName]
             , VarP xName
             ]
             $ ConE 'Entity `AppE` VarE keyVar `AppE` RecUpdE
@@ -1425,20 +1424,20 @@ mkLensClauses mps entDef = do
                 [(fieldName, VarE xName)]
 
     toSumClause lens' keyVar valName xName fieldDef = normalClause
-        [ConP (filterConName mps entDef fieldDef) []]
+        [conPCompat (filterConName mps entDef fieldDef) []]
         (lens' `AppE` getter `AppE` setter)
       where
         emptyMatch = Match WildP (NormalB $ VarE 'error `AppE` LitE (StringL "Tried to use fieldLens on a Sum type")) []
         getter = LamE
-            [ ConP 'Entity [WildP, VarP valName]
+            [ conPCompat 'Entity [WildP, VarP valName]
             ] $ CaseE (VarE valName)
-            $ Match (ConP (sumConstrName mps entDef fieldDef) [VarP xName]) (NormalB $ VarE xName) []
+            $ Match (conPCompat (sumConstrName mps entDef fieldDef) [VarP xName]) (NormalB $ VarE xName) []
 
             -- FIXME It would be nice if the types expressed that the Field is
             -- a sum type and therefore could result in Maybe.
             : if length (getUnboundFieldDefs entDef) > 1 then [emptyMatch] else []
         setter = LamE
-            [ ConP 'Entity [VarP keyVar, WildP]
+            [ conPCompat 'Entity [VarP keyVar, WildP]
             , VarP xName
             ]
             $ ConE 'Entity `AppE` VarE keyVar `AppE` (ConE (sumConstrName mps entDef fieldDef) `AppE` VarE xName)
@@ -2360,7 +2359,7 @@ mkUniqueKeys def = do
             x' <- newName $ '_' : unpack (unFieldNameHS x)
             return (x, x')
         let pcs = fmap (go xs) $ entityUniques $ unboundEntityDef def
-        let pat = ConP
+        let pat = conPCompat
                 (mkEntityDefName def)
                 (fmap (VarP . snd) xs)
         return $ normalClause [pat] (ListE pcs)
@@ -2549,7 +2548,7 @@ mkField mps entityMap et fieldDef = do
             maybeIdType mps entityMap fieldDef Nothing Nothing
     bod <- mkLookupEntityField et (unboundFieldNameHS fieldDef)
     let cla = normalClause
-                [ConP name []]
+                [conPCompat name []]
                 bod
     return $ EntityFieldTH con cla
   where
@@ -2579,7 +2578,7 @@ mkIdField mps ued = do
                 [mkEqualP (VarT $ mkName "typ") entityIdType]
                 $ NormalC name []
         , entityFieldTHClause =
-            normalClause [ConP name []] clause
+            normalClause [conPCompat name []] clause
         }
 
 lookupEntityField
@@ -2658,7 +2657,7 @@ mkJSON mps (fixEntityDef -> def) = do
             typeInstanceD ''ToJSON (mpsGeneric mps) typ [toJSON']
           where
             toJSON' = FunD 'toJSON $ return $ normalClause
-                [ConP conName $ fmap VarP xs]
+                [conPCompat conName $ fmap VarP xs]
                 (objectE `AppE` ListE pairs)
               where
                 pairs = zipWith toPair fields xs
@@ -2670,7 +2669,7 @@ mkJSON mps (fixEntityDef -> def) = do
             typeInstanceD ''FromJSON (mpsGeneric mps) typ [parseJSON']
           where
             parseJSON' = FunD 'parseJSON
-                [ normalClause [ConP 'Object [VarP obj]]
+                [ normalClause [conPCompat 'Object [VarP obj]]
                     (foldl'
                         (\x y -> InfixE (Just x) apE' (Just y))
                         (pureE `AppE` ConE conName)
@@ -3132,3 +3131,10 @@ setNull (fd :| fds) =
         else error $
             "foreign key columns must all be nullable or non-nullable"
            ++ show (fmap (unFieldNameHS . unboundFieldNameHS) (fd:fds))
+
+conPCompat :: Name -> [Pat] -> Pat
+conPCompat n pats = ConP n
+#if MIN_VERSION_template_haskell(2,18,0)
+                         []
+#endif
+                         pats
